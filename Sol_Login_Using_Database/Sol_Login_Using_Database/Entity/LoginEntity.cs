﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Login_Using_Database.Entity
{
    public class LoginEntity
    {
        public int UserId { get; set; }

        public String Email { get; set; }

        public String Password { get; set; }
    }
}