﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Login_Using_Database.Entity
{
    public class UserInfoEntity
    {
        public int UserId { get; set; }

        public String FirStName { get; set; }

        public String LastName { get; set; }

        public LoginEntity EntityLogin { get; set; }
    }
}