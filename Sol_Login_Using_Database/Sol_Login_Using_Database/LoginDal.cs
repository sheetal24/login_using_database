﻿using Sol_Login_Using_Database.ORD;
using Sol_Login_Using_Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace Sol_Login_Using_Database
{
    public class LoginDal
    {
        #region private declaration

        private LoginDCDataContext dc = null;

        #endregion

        #region constructor 

        LoginDal()
        {
            dc = new LoginDCDataContext();
        }
        #endregion
        #region public methods
        
        public async Task<bool>Validation(UserInfoEntity userinfoentityObj)
        {
            int? status = null;
            return await Task.Run(() =>
            {

                var setQuery =
                dc
                ?.uspUserLogin(
                   
                    , userinfoentityObj?.EntityLogin?.Email
                    , userinfoentityObj?.EntityLogin?.Password
                    
                    );
                return (status == 1) ? true : false;

            });
             
                    

        }
        #endregion

    }
}